// 1. Create an object representation of yourself
// Should include: 
// - firstName
// - lastName
// - 'favorite food'
// - bestFriend (object with the same 3 properties as above)
const bestFrient = {
  firstName : 'Helen',
  lastName : 'Keller',
  'favorite food' : 'Spinach'
}
const mySelf = {
  firstName : 'Jessica',
  lastName : 'Chan',
  'favorite food' : 'Chocolate',
  bestFrient
}

// 2. console.log best friend's firstName and your favorite food
console.log('------------ #2 ------------------');
console.log(`My best frient is ${mySelf.bestFrient.firstName} and my favorite food is ${mySelf["favorite food"]}`);

// 3. Create an array to represent this tic-tac-toe board
// -O-
// -XO
// X-X
const r1 = ['-','O','-'];
const r2 = ['-','X','O'];
const r3 = ['X','-','X'];
let ticTacToe = [ r1, r2, r3];

// 4. After the array is created, 'O' claims the top right square.
// Update that value using MUTATOR method:
r1.pop();
r1.push('X');
// Update that value directly by using INDEX in bracket notation:
ticTacToe[0][2] = 'O';

// 5. Log the grid to the console.
console.log('------------ #5 ------------------');
console.log(`Tic Tac Toe Board: \n ${ticTacToe[0]}  \n ${ticTacToe[1]}  \n ${ticTacToe[2]}`);

// 6. You are given an email as string myEmail, make sure it is in correct email format.
// Should be 1 or more characters, then @ sign, then 1 or more characters, then dot, then one or more characters - no whitespace
// i.e. foo@bar.baz
// Hints:
// - Use rubular to check a few emails: https://rubular.com/
// - Use regexp test method https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test

/* username@domain.top-level-domain */
const regex = /^\w+([-#%&()\.\~\!\$\^\*]?\w+)*@\w+([\-]?\w+)*[.][a-z A-Z]{2,3}$/;

let myEmail;
myEmail = "!nvalid.email..foo$me@yahoo.com";
myEmail = "val!d.email$canta#in(f*o)_me2@yahoo-mail.io"; 

let test = regex.test(myEmail);

console.log(`------------ #6 : test expression = ${regex} -----------`);
console.log(`${myEmail} is ${test}`);

// 7. You are given an assignmentDate as a string in the format "month/day/year"
// i.e. '1/21/2019' - but this could be any date.
// Convert this string to a Date
const assignmentDate = '1/21/2019';
const aDate=new Date(assignmentDate);

// 8. Create a new Date instance to represent the dueDate.  
// This will be exactly 7 days after the assignment date.
let dueDate = new Date();
const addDays = 7;
dueDate = new Date(aDate.getFullYear(), aDate.getMonth(), aDate.getDate()+ addDays);

//or use setTime()
let durationTime = addDays*24*60*60*1000;
dueDate.setTime(aDate.getTime() + durationTime);

//or use setDate()
dueDate.setDate(aDate.getDate() + addDays);

console.log('------------ #8 ------------------');
console.log(`Due Date is ${dueDate}`);

// 9. Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// I have provided a months array to help
const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];

let dMonth = months[dueDate.getMonth()];
let dMon = dueDate.getMonth()+1;
let dDate = dueDate.getDate();
let dYear = dueDate.getFullYear();
let html = `<time datetime="${dYear}-${dMon.toString().padStart(2,'0')}-${dDate.toString().padStart(2,'0')}"> ${dMonth} ${dDate}, ${dYear}</time>`;

// 10. log this value using console.log
console.log('------------ #10 ------------------');
console.log(html);
document.body.style.backgroundColor = "lightgreen";
document.body.innerHTML = "<h2>Due date is <i>" + html + "</i></h2>";
